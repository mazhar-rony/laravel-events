<?php

namespace Database\Seeders;

use Faker\Factory;
use App\Models\Post;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // $fake   = Factory::create();
        // $userId = DB::table('users')->pluck('id')->toArray();

        // for($i = 0; $i < 19; $i++) {
        //     $randomValue = array_rand($userId);
        //     DB::table('posts')->insert([
        //         'uuid'          => $fake->uuid(),
        //         'user_id'       => $userId[$randomValue],
        //         'created_at'    => now(),
        //         'updated_at'    => now(),
        //         'description'   => $fake->paragraph($nbSentences = 5, $variableNbSentences = true),
        //         'view_count'    => 0
        //     ]);
        // }

        Post::factory(10)->create();
    }
}
