<?php

namespace App\Http\Controllers\Auth;

use Exception;
use App\Models\User;
use Illuminate\View\View;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\RedirectResponse;
use Illuminate\Auth\Events\Registered;
use App\Providers\RouteServiceProvider;
use Illuminate\Database\QueryException;
use App\Http\Requests\RegisterUserRequest;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     */
    public function create(): View
    {
        return view('auth.register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(RegisterUserRequest $request): RedirectResponse
    {
        $validatedFields = $request->validated();
        
        $name = explode(' ', $validatedFields['name']);
        $last_name = null;

        if(sizeof($name) > 1) {
            $last_name = array_pop($name);
        }

        $first_name = implode(' ', $name);

        $validatedFields['password'] = Hash::make($validatedFields['password']);

        unset($validatedFields['name']);
        
        $validatedFields = array_merge([
            'uuid'          => Str::uuid(),
            'first_name'    => $first_name,
            'last_name'     => $last_name,
        ], $validatedFields);
        
        try {
            $user = User::create($validatedFields);

            event(new Registered($user));

            // Log in the user
            Auth::login($user);

            Toastr::success('User registration successfull!', 'Success');

            return redirect(RouteServiceProvider::HOME);
            
        } catch (QueryException | Exception $e) {
            Log::error($e->getMessage());
            
            return redirect()->back()->withInput()->withErrors(
                config('app.env') == 'production' ? __('Somethings Went Wrong') : $e->getMessage()
            );
        }
    }
}
