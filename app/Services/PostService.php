<?php

namespace App\Services;

use App\Models\Post;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\Constants\MediaCollections;
use Illuminate\Support\Facades\Auth;

class PostService
{
    public function store(array $data, $image = null): void
    {
        DB::transaction(function () use ($data, $image) {
            $uuid       = Str::uuid();
            $userId     = Auth::id();

            $data = array_merge([
                'uuid'          => $uuid,
                'user_id'       => $userId,
            ], $data);

            $post = Post::create($data);

            if ($image) {
                $post->clearMediaCollection(MediaCollections::POST_IMAGES);
                $post->addMedia($image)->toMediaCollection(MediaCollections::POST_IMAGES);
            }
        }, 5);
    }

    public function update(Post $post, array $data, $image = null): void
    {
        DB::transaction(function () use ($post, $data, $image) {
            
            $post = tap($post)->update($data);

            if ($image) {
                $post->clearMediaCollection(MediaCollections::POST_IMAGES);
                $post->addMedia($image)->toMediaCollection(MediaCollections::POST_IMAGES);
            }
        }, 5);
    }

    public function destroy(Post $post): void
    {
        $post->delete();
    }
}
